provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "db" {
  ami           = var.ami
  instance_type = var.type

  tags = {
      Name = "DB Server"
  }
}

resource "aws_instance" "webserver" {
  ami           = var.ami
  instance_type = var.type
  depends_on = [aws_instance.db]

}


data "aws_instance" "dbsearch" {
    filter {
        name = "tag:Name"
        values = ["DB Server"]
    }

}

output "dbservers" {
    value = data.aws_instance.dbsearch.availability_zone
}

output "public_ip" {
    value = data.aws_instance.dbsearch.public_ip
}



